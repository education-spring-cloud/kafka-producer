# Kafka producer

Suppose there is record in etc/hosts which maps ip address of virtual machine where runs docker (kafka, manager, ...) to name **kafka01**. 
E.g.:
```
192.168.56.101 kafka01
```
The DNS name `kafka01` is used in this project config.

## Kafka in docker
Image: https://hub.docker.com/r/spotify/kafka.
This image contains Kafka with Zookeeper in one image.

run it like this:
```
docker run -d --name kafka01 -p 2181:2181 -p 9092:9092 --hostname kafka01 spotify/kafka
``` 

Kafka manager image: https://hub.docker.com/r/sheepkiller/kafka-manager.
run it:
```
docker run -d --name manager01 -p 9000:9000 -e ZK_HOSTS="kafka01:2181" --hostname manager01 --link kafka01:kafka01 sheepkiller/kafka-manager
```
Kafka manager is now available on http://localhost:9000. So...
- Create new cluster in manager app with arbitrary name and value `kafka01:2181` of Zookeeper hosts.
- Create new topic in manager



